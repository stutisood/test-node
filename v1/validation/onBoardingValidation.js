const joi = require("joi");

const signUpValidate = joi.object({
  username: joi.string().min(3).max(30).required(),
  email: joi.string().email().required(),
  password: joi.string().required(),
});

const loginValidate = joi.object({
  email: joi.string().email().required(),
  password: joi.string().required(),
});

module.exports = {
  signUpValidate,
  loginValidate,
};
