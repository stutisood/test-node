const joi = require("joi");

const foodStoreValidate = joi.object({
  storeName: joi.string().min(3).max(30).required(),
  location: joi.string().email().required(),
  menu: joi.string().required(),
});

const placeOrderValidate = joi.object({
  item: joi.string().required(),
  address: joi.string().required(),
});

module.exports = {
  foodStoreValidate,
  placeOrderValidate,
};
