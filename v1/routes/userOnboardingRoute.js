const express = require("express");
const controller = require("../controller/userOnboarding");
const userRouter = express.Router();

userRouter.post("/signUp", controller.signUp);
userRouter.post("/login", controller.login);
userRouter.post("/verify", controller.verify, controller.verifyToken);

module.exports = userRouter;
