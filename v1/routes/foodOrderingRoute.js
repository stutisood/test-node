const express = require("express");
const controller = require("../controller/foodOrdering");
const foodOrderRouter = express.Router();

foodOrderRouter.get("/getMenu/:storeId", controller.getMenu);
foodOrderRouter.post("/placeOrder/:storeId/:customerId", controller.placeOrder);
foodOrderRouter.get("/orderHistory/:customerId", controller.orderHistory);

module.exports = foodOrderRouter;
