const express = require("express");
const controller = require("../controller/foodStore");
const foodStoreRouter = express.Router();

foodStoreRouter.post("/registerStore", controller.registerStore);
foodStoreRouter.put("/setMenu/:storeId", controller.setMenu);

module.exports = foodStoreRouter;
