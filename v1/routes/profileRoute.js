const express = require("express");
const controller = require("../controller/profileManagement");
const profileRouter = express.Router();

profileRouter.get("/getProfile/:_id", controller.getProfile);
profileRouter.put("/updateProfile/:_id", controller.updateProfile);
profileRouter.post("/deactivateProfile/:_id", controller.deactivateProfile);

module.exports = profileRouter;
