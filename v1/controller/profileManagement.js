const signUpModel = require("../../models/userModels/signUpDb");
const profileModel = require("../../models/userModels/profileDb");

// 2. Profile Management:

// a) Retrieve Profile: Enable users to view and access their profile information post-login.

module.exports.getProfile = async (req, res) => {
  try {
    //Pagination with search
    const page = parseInt(req.query.page);
    const limit = parseInt(req.query.limit);
    const skip = (page - 1) * limit;

    let query = {};
    const searchKey = req.query.id;

    //Searching with query
    if (searchKey) {
      query.id = { $regex: req.query.searchKey };
    }

    const userProfile = await signUpModel
      .findOne(query)
      .skip(skip)
      .limit(limit);

    const docLength = await studentModel.countDocuments(query);

    return res.json({ userProfile, docLength });
  } catch {
    res.status(500).send("Internal Server Error");
  }
};

// b) Update Profile: Provide users with the option to modify their profile details, including profile picture, contact information, and preferences.

module.exports.updateProfile = async (req, res) => {
  try {
    const id = req.params;
    const update = await signUpModel.findOneAndUpdate(
      { _id: id },
      {
        $set: req.body,
      }
    );
  } catch (error) {
    console.error(error);
    res.status(500).send("Internal Server Error");
  }
};

// c) Deactivate Profile: Allow users to temporarily suspend their accounts without permanent deletion if desired.

module.exports.deactivateProfile = async (req, res) => {
  try {
    const deactivate = await profileModel.create({
      userId: req.params.id,
      activate: req.body.activate,
    });

    return res.json({ deactivate });
  } catch {}
};
