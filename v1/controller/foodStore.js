const foodStoreModel = require("../../models/foodAppModel/foodStoreDb");
const { foodStoreValidate } = require("../validation/foodStoreValidation");
const menuModel = require("../../models/foodAppModel/menuDb");

// 3.Food Store Management:

// a) Register Store: Enable food stores to register on the platform by providing necessary information such as store name, location, and menu.

module.exports.registerStore = async (req, res) => {
  try {
    await foodStoreValidate.validateAsync(req.body);
    const store = await foodStoreModel.create({
      storeName: req.body.storeName,
      location: req.body.location,
      menu: req.body.menu,
    });

    return res.json({ store });
  } catch {
    res.status(500).send("Internal Server Error");
  }
};

// b) Manage Menu: Allow stores to update their menu items, prices, and availability.

module.exports.setMenu = async (req, res) => {
  const id = req.params.storeId;

  try {
    const menu = await menuModel.updateOne(
      { storeId: id },
      { $set: { price: req.body.price, menuItem: req.body.menuItem } }
    );

    res.json({ menu });
  } catch (error) {
    console.error(error);
    res.status(500).send("Internal Server Error");
  }
};
