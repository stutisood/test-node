const express = require("express");
const router = express.Router();
const menuModel = require("../../models/foodAppModel/menuDb");
const foodStoreModel = require("../../models/foodAppModel/foodStoreDb");
const orderModel = require("../../models/foodAppModel/orderDb");
const { placeOrderValidate } = require("../validation/foodStoreValidation");

// 4. Food Ordering:

// a)Browse Menu: Enable customers to browse through the list of available food items from various stores.
module.exports.getMenu = async (req, res) => {
  const storeId = req.params;

  //Aggregitgation
  const aggregate = await menuModel.aggregate([
    {
      $match: { _id: storeId },
    },

    {
      $lookup: {
        from: "foodStoreModel",
        localField: "_id",
        foreignField: "storeId",
        as: "menu",
      },
    },

    {
      $unwind: "$menu",
    },

    {
      $sort: { createdAt: -1 },
    },

    {
      $project: {
        "menu.storeId": false,
        _id: false,
      },
    },
  ]);
  return res.json({ aggregate });
};

// b) Place Order: Allow customers to select items from the menu and place orders.

module.exports.placeOrder = async (req, res) => {
  try {
    await placeOrderValidate.validateAsync(req.body);
    const food = await orderModel.create({
      storeId: req.params.storeId,
      customerId: req.params.customerId,
      items: req.body.items,
      address: req.body.address,
    });
    res.json({ msg: " Your Order Placed", food });
  } catch {
    res.status(500).send("Internal Server Error");
  }
};

// c) Order History: Provide customers with a history of their past orders for reference.
module.exports.orderHistory = async (req, res) => {
  try {
    const id = req.params;
    const history = await orderModel.findOne({ customerId: id });
    res.json({ history });
  } catch {
    res.status(500).send("Internal Server Error");
  }
};
