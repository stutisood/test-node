const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const { v4: uuidv4 } = require("uuid");
const secretKey = "secretKey";

const signUpModel = require("../../models/userModels/signUpDb");
const {
  signUpValidate,
  loginValidate,
} = require("../validation/onBoardingValidation");
const { agenda } = require("../../service/mail");
const sendMail = require("../../service/mail");

// 1. User Onboarding

// a) Signup: Enable users to register for an account by providing essential details such as username, email, and password.
module.exports.signUp = async (req, res) => {
  try {
    await signUpValidate.validateAsync(req.body);

    let password = req.body.password;
    //Hashed Password with Bcrypt
    let hashPassword = await bcrypt.hash(password, 10);

    const signUpDetail = await signUpModel.create({
      username: req.body.username,
      email: req.body.email,
      password: hashPassword,
    });

    let emailId = req.body.email;

    //NodeMailer with agenda
    sendMail(emailId);

    return res.json({ signUpDetail });
  } catch {
    res.status(401).send("Please check your credentials");
  }
};

// b) Login: Allow existing users to access their accounts using their credentials.

module.exports.login = async (req, res) => {
  try {
    await loginValidate.validateAsync(req.body);
    const email = req.body.email;
    const password = req.body.password;

    const existingUser = await signUpModel.findOne({ email: email });

    if (!existingUser) {
      return res.status(500).json({
        Message: "User not found",
      });
    }

    const cmpPwd = existingUser.password;
    const comparePassword = bcrypt.compare(password, cmpPwd);
    const id = existingUser._id;

    if (comparePassword) {
      const jti = uuidv4();

      await signUpModel.updateOne({ _id: id }, { jti: jti });
      
      //JWT token Generated with JTI
      jwt.sign(
        { id: id, jti },
        secretKey,
        { expiresIn: "24h" },
        (err, token) => {
          return res.json({
            token,
          });
        }
      );
    }
  } catch {
    res.status(401).send("Please check your credentials");
  }
};

//Token Verification
module.exports.verifyToken = (req, res) => {
  jwt.verify(req.token, secretKey, async (err, authData) => {
    if (err) {
      res.send({
        result: "Token is invalid",
      });
    } else {
      let data = await signUpModel.findOne(
        { _id: authData.id },
        { jti: authData.jti }
      );
      res.json({
        message: "Loggedin Successfully",
        data,
      });
      res.send(data);
    }
  });
};

module.exports.verify = (req, res, next) => {
  const bearerHeader = req.headers["authorization"];
  if (typeof bearerHeader !== "undefined") {
    const bearer = bearerHeader.split(" ");
    const token = bearer[1];

    req.token = token;
    next();
  } else {
    res.send({
      result: "Token is not valid",
    });
  }
};
