const chatModel = require("../models/chatModel/chatDb");
const enquiryModel = require("../models/chatModel/orderEnquiryChatDb");

// 4. Chat Functionality:
// Store-Customer Communication: Facilitate real-time chat between customers and stores to address order inquiries, customization requests, and delivery updates.

module.exports = (io) => {
  io.on("connection", (socket) => {
    console.log("User Connected");

    socket.on("join_room", async (data) => {
      const storeId = data.storeId;
      const customerId = data.customerId;

      const findUser = await chatModel.findOne({
        $and: [{ storeId: storeId }, { customerId: customerId }],
      });

      if (!findUser) {
        const joinRoom = await chatModel.create(data);
        console.log(joinRoom);

        const roomId = joinRoom._id;
        socket.join(roomId);
      } else {
        console.log("Room is already joined");
      }
    });

    socket.on("send_message_user", async (data) => {
      try {
        roomId = data.roomId;
        const findRoom = await enquiryModel.findOne({ _id: roomId });
        const msg = await enquiryModel.create({
          roomId: data.roomId,
          customerMessage: data.customerMessage,
          storeMessage: data.storeMessage,
        });
        console.log(msg);
      } catch {
        console.log("Room is not valid");
      }
    });

    socket.on("disconnect", function () {
      console.log("User disconnect");
    });
  });
};
