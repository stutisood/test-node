const express = require("express");
const http = require("http");

const userRouter = require("./v1/routes/userOnboardingRoute");
const profileRouter = require("./v1/routes/profileRoute");
const foodStoreRouter = require("./v1/routes/foodStoreRoute");
const foodOrderRouter = require("./v1/routes/foodOrderingRoute");

const app = express();
app.use(express.json());
app.use(userRouter);
app.use(profileRouter);
app.use(foodStoreRouter);
app.use(foodOrderRouter);

const server = http.createServer(app);
const io = require("socket.io")(server);
const socket = require("./sockets/socket");

const swaggerUi = require("swagger-ui-express");
const YAML = require("yamljs");
const { createSwagger } = require("./service/swagger");

require("./common/connection");

const swaggerDocument = YAML.load("./swagger/collection.yml");

server.listen(5000, async () => {
  socket(io);
  await createSwagger();
  app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
  console.log("server started at port 5000");
});
