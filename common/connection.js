const mongoose = require("mongoose");

mongoose
  .connect("mongodb://127.0.0.1:27017/FoodOrder")
  .then(function (db) {
    console.log("database connected");
  })
  .catch(function (err) {
    console.log(err, "Some Error");
  });
