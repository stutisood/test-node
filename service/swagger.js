const postmanToOpenApi = require("postman-to-openapi");
async function createSwagger() {
  try {
    const postmanCollection = "./swagger/collection.json";
    const outputFile = "./swagger/collection.yml";
    await postmanToOpenApi(postmanCollection, outputFile, {
      defaultTag: "General",
      openapiVersion: "3.0.0",
    });
    console.log("Swagger file created");
    return outputFile;
  } catch (error) {
    console.error(error);
    throw error;
  }
}
module.exports = { createSwagger };
