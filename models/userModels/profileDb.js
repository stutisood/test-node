const mongoose = require("mongoose");

const profileSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "signUpModel",
    },

    activate: {
      type: String,
    },
  },

  { timestamps: true }
);

const profileModel = mongoose.model("profileModel", profileSchema);
module.exports = profileModel;
