const mongoose = require("mongoose");

const signUpSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      required: true,
    },

    email: {
      type: String,
      required: true,
    },

    password: {
      type: String,
      required: true,
    },

    jti: {
      type: String,
    },
  },

  { timestamps: true }
);

const signUpModel = mongoose.model("signUpModel", signUpSchema);
module.exports = signUpModel;
