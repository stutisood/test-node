const mongoose = require("mongoose");

const foodStoreSchema = new mongoose.Schema(
  {
    storeName: {
      type: String,
    },

    location: {
      type: String,
    },

    menu: [
      {
        type: String,
      },
    ],
  },

  { timestamps: true }
);

const foodStoreModel = mongoose.model("foodStoreModel", foodStoreSchema);
module.exports = foodStoreModel;
