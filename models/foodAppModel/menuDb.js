const mongoose = require("mongoose");

const menuSchema = new mongoose.Schema(
  {
    storeId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "foodStoreModel",
    },

    menuItem: [
      {
        type: String,
      },
    ],

    price: [
      {
        type: String,
      },
    ],

    availability: {
      type: String,
    },
  },

  { timestamps: true }
);

const menuModel = mongoose.model("menuModel", menuSchema);
module.exports = menuModel;
