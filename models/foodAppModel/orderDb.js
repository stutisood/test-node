const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema(
  {
    storeId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "foodStoreModel",
    },

    customerId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "signUpModel",
    },

    items: [
      {
        type: String,
        required: true,
      },
    ],

    address: {
      type: String,
      required: true,
    },

    orderDate: {
      type: Date,
      default: Date.now,
    },
  },
  { timestamps: true }
);

const orderModel = mongoose.model("orderModel", orderSchema);

module.exports = orderModel;
