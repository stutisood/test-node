const mongoose = require("mongoose");

const chatSchema = new mongoose.Schema(
  {
    storeId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "foodStoreModel",
    },

    customerId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "signUpModel",
    },
  },

  { timestamps: true }
);

const chatModel = mongoose.model("chatModel", chatSchema);
module.exports = chatModel;
