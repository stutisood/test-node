const mongoose = require("mongoose");

const enquirySchema = new mongoose.Schema(
  {
    roomId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "chatModel",
    },

    customerMessage: {
      type: String,
    },

    storeMessage: {
      type: String,
    },
  },

  { timestamps: true }
);

const enquiryModel = mongoose.model("enquiryModel", enquirySchema);
module.exports = enquiryModel;
